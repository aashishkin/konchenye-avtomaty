﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace конченые_автоматы
{
    class Program
    {
        static void Main(string[] args)
        {
            double i = 0, d = 0, m = 0, n = 0;
            Console.Write("Количество автоматов: ");
            int k = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            while (i<k)
            {
                Console.Write($"Количество состояний {i+1} автомата: ");
                n = int.Parse(Console.ReadLine());
                Console.Write($"Количество переходов {i+1} автомата: ");
                m = int.Parse(Console.ReadLine());
                d = (19 * m) + (((n + 239) * (n + 366)) / 2);
                Console.WriteLine($"Нетривиальность {i+1} автомата равна {d}");
                Console.WriteLine("");
                i++;
            }
            Console.Write("Для выхода нажмите любую клавишу...");
            Console.ReadKey();
        }
    }
}
